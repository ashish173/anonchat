// KnockKnockServer.java

import java.net.*;
import java.io.*;

public class KnockKnockServer {
    public static void main(String[] args) throws IOException{
        ServerSocket serverSocket = null;    // creates server socket 
        
        try {  // starts a server socket
            serverSocket = new ServerSocket(4444);  // socket listens on 4444
        } catch(IOException e) {
            System.err.println("Could not listen on port : 4444");
            System.exit(1);
        }

        Socket clientSocket = null;  // normal socket
        
        try {  // serverSocket accepts a connection and stores in clientSocket
            clientSocket = serverSocket.accept();
        } catch(IOException e) {
            System.out.println("Accept Failed");
            System.exit(1);
        }

        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(
                                                clientSocket.getInputStream()));
        String inputLine, outputLine;
        KnockKnockProtocol kkp = new KnockKnockProtocol();

        outputLine = kkp.processInput(null);
        out.println(outputLine);

        while((inputLine = in.readLine()) != null) {
            outputLine = kkp.processInput(inputLine);
            out.println(outputLine);
            if(outputLine.equals("Bye."))
                break;
        }
        out.close();
        in.close();
        clientSocket.close();
        serverSocket.close();
    }
}
